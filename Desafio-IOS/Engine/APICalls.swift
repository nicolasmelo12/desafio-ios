//
//  APICalls.swift
//  Desafio-IOS
//
//  Created by Nicolas Melo on 20/1/18.
//  Copyright © 2018 Nicolas Melo. All rights reserved.
//

import Foundation
import Alamofire


public func GET(url: String, completionHandler: @escaping (Data?, Error?) -> ()){
    Alamofire.request(url, method: .get, encoding: JSONEncoding.default)
        .responseJSON{ response in
            
            //to get JSON return value
            if let result = response.data {
                completionHandler(result, nil)
            }
            
    }
}
