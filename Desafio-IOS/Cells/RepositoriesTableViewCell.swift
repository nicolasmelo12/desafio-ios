//
//  RepositoriesTableViewCell.swift
//  Desafio-IOS
//
//  Created by Nicolas Melo on 20/1/18.
//  Copyright © 2018 Nicolas Melo. All rights reserved.
//

import UIKit

class RepositoriesTableViewCell: UITableViewCell {
    @IBOutlet var starIcon: UIImageView!
    @IBOutlet var forkIcon: UIImageView!
    @IBOutlet var userProfilePic: UIImageView!
    @IBOutlet var firstAndLastNames: UILabel!
    @IBOutlet var username: UILabel!
    @IBOutlet var numberOfStars: UILabel!
    @IBOutlet var numberOfForks: UILabel!
    @IBOutlet var repositoryDescription: UILabel!
    @IBOutlet var repositoryName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        userProfilePic.layer.masksToBounds = false
        userProfilePic.layer.cornerRadius = userProfilePic.frame.size.height/2
        userProfilePic.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
