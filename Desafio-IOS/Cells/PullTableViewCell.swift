//
//  PullTableViewCell.swift
//  Desafio-IOS
//
//  Created by Nicolas Melo on 22/1/18.
//  Copyright © 2018 Nicolas Melo. All rights reserved.
//

import UIKit

class PullTableViewCell: UITableViewCell {
    
    @IBOutlet var userImage: UIImageView!
    @IBOutlet var nomeESobrenome: UILabel!
    @IBOutlet var username: UILabel!
    @IBOutlet var pullDescription: UILabel!
    @IBOutlet var pullTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        userImage.layer.masksToBounds = false
        userImage.layer.cornerRadius = userImage.frame.size.height/2
        userImage.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
