//
//  PullTableViewController.swift
//  Desafio-IOS
//
//  Created by Nicolas Melo on 22/1/18.
//  Copyright © 2018 Nicolas Melo. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class PullTableViewController: UITableViewController {
    var urlPulls: String?
    var titulo: String?
    var pulls = [Pulls]()
    @IBOutlet var numberOfPulls: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = titulo
        loadPulls()

        tableView.tableFooterView = UIView()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
            return pulls.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "PullTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! PullTableViewCell
        let pull = pulls[indexPath.row]
        cell.userImage.kf.setImage(with: pull.userProfilePic)
        cell.nomeESobrenome.text = pull.fullName
        cell.pullDescription.text = pull.description
        cell.pullTitle.text = pull.pullName
        cell.username.text = pull.login
        

        return cell
    }

    
    
    private func loadPulls(){
        GET(url: urlPulls!.components(separatedBy:"{")[0]+"?client_id=2efdc93bd5c143aa15f9&client_secret=57a667ba1851b1698192f77952bccfbf62022d25"){ responseObject,error in
            for (_, subJson): (String, JSON) in JSON(responseObject!){
                let pull = Pulls(
                    userProfilePic: URL(string: subJson["user"]["avatar_url"].stringValue)!,
                    pullName: subJson["number"].stringValue,
                    fullName: subJson["user"]["url"].stringValue,
                    login: subJson["user"]["login"].stringValue,
                    description: subJson["title"].stringValue,
                    urlToOpen: subJson[]["html_url"].stringValue)
                self.pulls.append(pull)
            }
            self.numberOfPulls.text = String(self.pulls.count) + " pull requests"
            for (index, pull) in self.pulls.enumerated(){
                GET(url: pull.fullName+"?client_id=2efdc93bd5c143aa15f9&client_secret=57a667ba1851b1698192f77952bccfbf62022d25"){responseObject, error in
                    self.pulls[index].fullName = JSON(responseObject!)["name"].stringValue
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UIApplication.shared.openURL(URL(string: pulls[indexPath.row].urlToOpen)!)
    }
    
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
