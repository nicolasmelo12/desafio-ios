//
//  RepositoriesTableViewController.swift
//  Desafio-IOS
//
//  Created by Nicolas Melo on 20/1/18.
//  Copyright © 2018 Nicolas Melo. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import Kingfisher

class RepositoriesTableViewController: UITableViewController {
    
    var count = 1
    var repositories=[Repositories]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadInfosRepositories(page: count)
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositories.count
    }

    
    
   
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "RepositoriesTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! RepositoriesTableViewCell
        
        let repository = repositories[indexPath.row]
        cell.userProfilePic.kf.setImage(with: repository.userImage)
        cell.username.text = repository.login
        cell.numberOfStars.text = repository.stargazeCount
        cell.repositoryName.text = repository.name
        cell.numberOfForks.text = repository.forksCount
        cell.repositoryDescription.text = repository.description
        cell.firstAndLastNames.text = repository.fullName
        cell.forkIcon.image = UIImage(named: "ForkIcon")
        cell.starIcon.image = UIImage(named: "StarIcon")
        return cell
        
    }
 
    private func loadInfosRepositories(page: Int){
    
        
        GET(url: "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=" + String(page)){ responseObject,error in
            for (_, subJson): (String, JSON) in JSON(responseObject!)["items"] {
                let repository = Repositories(
                    userImage: URL(string: subJson["owner"]["avatar_url"].stringValue)!,
                    description: subJson["description"].stringValue,
                    forksCount: subJson["forks_count"].stringValue,
                    stargazeCount: subJson["stargazers_count"].stringValue,
                    name: subJson["name"].stringValue,
                    fullName: "",
                    urlToGetName: subJson["owner"]["url"].stringValue,
                    login: subJson["owner"]["login"].stringValue,
                    pullsUrl: subJson["pulls_url"].stringValue)
                
               
                self.repositories.append(repository)
            }
            print(self.repositories)
            for (index, repository) in self.repositories.enumerated(){
                GET(url: repository.urlToGetName+"?client_id=2efdc93bd5c143aa15f9&client_secret=57a667ba1851b1698192f77952bccfbf62022d25"){responseObject, error in
                    self.repositories[index].fullName = JSON(responseObject!)["name"].stringValue
                }
            }
            self.tableView.reloadData()
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if ((scrollView.contentOffset.y + scrollView.bounds.height == scrollView.contentSize.height) == true){
            count = count + 1
            loadInfosRepositories(page: count)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let ident = segue.identifier {
            switch ident {
            case "PullTableViewController":
                let backButton = UIBarButtonItem()
                backButton.title = "Voltar"
                backButton.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                navigationItem.backBarButtonItem = backButton
                
                guard let pullTableViewController = segue.destination as? PullTableViewController else {
                    fatalError("Unexpected destination: \(segue.destination)")
                }
                
                guard let indexPath = tableView.indexPathForSelectedRow else {
                    fatalError("The selected cell is not being displayed by the table")
                }
                
                let repository: Repositories
                /*if searchController.isActive && searchController.searchBar.text != "" {
                    repository = filteredComidinhas[indexPath.row]
                } else {*/
                    repository = repositories[indexPath.row]
                //}
                
                pullTableViewController.urlPulls = repository.pullsUrl
                pullTableViewController.titulo = repository.name
                
            default: break
            }
        }
        
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
