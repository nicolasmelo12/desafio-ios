//
//  Repositories.swift
//  Desafio-IOS
//
//  Created by Nicolas Melo on 21/1/18.
//  Copyright © 2018 Nicolas Melo. All rights reserved.
//
import UIKit

class Repositories {
    //Mark: Nomes
    var userImage: URL
    var forksCount: String
    var stargazeCount: String
    var name: String
    var fullName: String
    var urlToGetName: String
    var login: String
    var description: String
    var pullsUrl: String
    
    init (
            userImage: URL,
            description: String,
            forksCount: String,
            stargazeCount: String,
            name: String,
            fullName: String,
            urlToGetName: String,
            login: String,
            pullsUrl: String){
        
        self.urlToGetName = urlToGetName
        self.userImage = userImage
        self.description = description
        self.forksCount = forksCount
        self.stargazeCount = stargazeCount
        self.name = name
        self.fullName = fullName
        self.login = login
        self.pullsUrl = pullsUrl
    }
    
}
