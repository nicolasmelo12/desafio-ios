//
//  Pulls.swift
//  Desafio-IOS
//
//  Created by Nicolas Melo on 22/1/18.
//  Copyright © 2018 Nicolas Melo. All rights reserved.
//

import Foundation

class Pulls {
    //Mark: Nomes
    var userProfilePic: URL
    var pullName: String
    var fullName: String
    var login: String
    var description: String
    var urlToOpen: String
    
    init (
        userProfilePic: URL,
        pullName: String,
        fullName: String,
        login: String,
        description: String,
        urlToOpen: String){
        
        
        self.userProfilePic = userProfilePic
        self.description = description
        self.pullName = pullName
        self.fullName = fullName
        self.login = login
        self.urlToOpen = urlToOpen
    }
    
}
